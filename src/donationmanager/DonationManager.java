/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package donationmanager;

/**
 *
 * @author kelsey.pritsker676
 */
public class DonationManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DonationModel model = new DonationModel();
        DonationView view = new DonationView();
        
        DonationController controller = new DonationController(view,model);
        
        view.setVisible(true);
    }
}
