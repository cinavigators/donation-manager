/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package donationmanager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author joel.helling904
 */
public class DonationController {
    private DonationModel model;
    private DonationView view;
    public DonationController(DonationView view, DonationModel model)
    {
        this.view = view;
        this.model = model;
        
        view.addListenerSubmit(new ListenerSubmit());
        view.addListenerCancel(new CancelSubmit());
    }
    
    private class CancelSubmit implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                view.setVisible(false);
                System.exit(0);
            } catch (NumberFormatException nfex) {
                //view.showError("Bad input: '" + amount + "'");
            }
        }
    }
    
    private class ListenerSubmit implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                String name = view.getName();
                String email = view.getEmail();
                int donation = view.getDonation();
                System.out.println(model.calculatePlague(donation));
                view.setVisible(false);
                view.dispose();
                boolean sentEmail = model.sendEmail(name, email,model.calculatePlague(donation));
                //System.out.printf("%s\n", sentEmail);
            } catch (NumberFormatException nfex) {
                //view.showError("Bad input: '" + amount + "'");
            }
        }
    }
}
